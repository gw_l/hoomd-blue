BMDir=../../hoomd-benchmarks
export PYTHONPATH=`pwd`/../build:$PYTHONPATH

cd $BMDir/lj-liquid/
cmd="python bmark.py"
nvprof --csv $cmd
#mvprof --query-events
#nvprof --query-metrics
#nvprof --events inst_executed $cmd
#nvprof --metrics all $cmd
#nvprof --metrics inst_executed $cmd
#nvprof --aggregate-mode-off --source-level-analysis global_access,shared_access,branch,instruction_execution,pc_sampling  $cmd

