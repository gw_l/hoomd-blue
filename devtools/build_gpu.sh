TOP=`pwd`/..
bDir="$TOP/build"
rm -fr $bDir
mkdir $bDir
cd  $bDir 
cmake ..  -DENABLE_CUDA=ON -DENABLE_MPI=ON
make -j
